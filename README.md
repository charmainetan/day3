##Prepare project structure (in Terminal)
1. Create a new working folder ```mkdir <project name>```
1. Create a sub-folder for server stuff ```mkdir <project name>/server```
1.Create a sub-folder for client files such as html & css pages `mkdir <project name>/client`

##Setup Bitbucket
1. Create new repo on bitbucket
1. Choose public or private access level

##Prepare Git
1. Initialize directory as a repository `git init`
1. Create an ignore-list for certain node_modules that should not appear on remote director `.gitignore`
1. Check which remote directory is the current directory configured to `git remote -v`
1. Remove remote URL from repository: `git remote remove origin`
1. Point the local repo to the remote repo: `git remote add origin <http git url>`
1. Add files to staging area: `git add . ` / `git add filename.type`
1. Commit changes to local repo: `git commit -m "comment"`
1. Push changes to remote repo: `git push -u origin master`
1. Check if branch is up-to-date with origin/master: `git status`

##Global Utility Installation
1. Use NPM to install Nodemon: `npm install -g nodemon`
1. Use NPM to install Bower: `npm install -g bower`

##Prepare Express JS
1. Initialise NPM in project directory: `npm init`
1. Install Express: `npm install express --save`

##How to start my app
1. Set a port number: `export NODE_PORT=3000`
1. Run nodemon to check: `nodemon or nodemon server/app.js`
1. Open browser to check app on at `localhost:3000`

##Bower
1. Create .bowerrc file in project directory. Add the following lines in .bowerrc file: `{"directory": "client/bower_components"}`
1. Initialise bower in terminal: `bower init`
1. Install Angular: `bower install angular --save`
1. Install Bootstrap: `bower install bootstrap --save`
1. Install FontAwesome: `bower install --save fontawesome`

Add to app.js:
app.use("/bower_components", express.static(__dirname + "/../client/bower_components/"));


###Other Notes
NPM and Bower are both dependency management tools. The main difference between both is NPM is used for installing Node js modules but Bower js is used for managing front end components like html, css, js etc. 
Handles dependencies to remove incompatibility issues (e.g. between jquery and bootstrap versions, etc.)