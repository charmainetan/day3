function hi1(name) {
    console.log("Hello! --" + name);
}

function hi2(name) {
    console.log("Hello2! --" + name);
}

function sayHi(callback, callback2, name) {
    console.log("Inside sayHi");
    callback(name);
    callback2(name);
}

sayHi(hi1, hi2, "charmaine")

var obj = {
    name: "Kenneth",
    age: 30,
    gender: "Male",
    say: function(prefix) {
        return prefix + this.name + " age -> " + this.age;
    }
};

var morphAttr = (name:"John", age:43);
var result = obj.say.call(morphAttr, "Good Morning");
console.log(result);