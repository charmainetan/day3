console.log("Starting...");
var express = require("express");

var app = express();
console.log(__dirname);
console.log(__dirname + "/../client/");

const NODE_PORT = process.env.NODE_PORT || 3000;

app.use(express.static(__dirname + "/../client/"));
// app.use("/bower_components", express.static(__dirname + "/../client/bower_components/"));

// virtual mapped to physical. /bower_components (virtual) v dirname+/../client/bower_components/ (physical)

if (!NODE_PORT) {
    console.log("Express Port is not set");
    process.exit();
}

// app.use(function(req, res, next) {
//     console.log("app use");
//     res.send("Sorry wrong door!");
// });

app.get("/users", function(req, res) {
    console.log ("users");
    var users = [{
        name: "Kenneth",
        age: 35
    }, {
        name: "Alvin",
        age: 40
    }];
    res.json(users);
});

app.use(function(req,res) {
    console.log("sorry wrong door ->");
    var x = 1 + 1;
    res.send("<h1>Sorry wrong door! " + x + "</h1>");
});

app.listen(NODE_PORT,function(){
    console.log("Web App started at " + NODE_PORT);
});

