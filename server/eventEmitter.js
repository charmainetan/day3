var events = require("events");

var eventEmitter = new events.EventEmitter ();

var ringBell = function(){
    console.log("ring ring ring");
};

var slamDoor = function() {
    console.log("BANG!");
};

console.log("Event Emitter");
eventEmitter.on("doorOpen", ringBell);
eventEmitter.on("closeDoor", slamDoor);
eventEmitter.emit("doorOpen");
eventEmitter.emit("closeDoor");
